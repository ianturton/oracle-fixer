package com.astuntechnology.oraclefixer;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.geotools.data.DataStore;
import org.geotools.data.DataStoreFinder;
import org.geotools.data.DataUtilities;
import org.geotools.data.DefaultTransaction;
import org.geotools.data.Query;
import org.geotools.data.Transaction;
import org.geotools.data.simple.SimpleFeatureCollection;
import org.geotools.data.simple.SimpleFeatureIterator;
import org.geotools.data.simple.SimpleFeatureSource;
import org.geotools.data.simple.SimpleFeatureStore;
import org.geotools.factory.CommonFactoryFinder;
import org.geotools.filter.text.cql2.CQLException;
import org.geotools.filter.text.ecql.ECQL;
import org.opengis.feature.simple.SimpleFeature;
import org.opengis.filter.Filter;
import org.opengis.filter.FilterFactory2;

import com.vividsolutions.jts.geom.Geometry;

public class FixAndReplace {
	private boolean verbose = false;
	private boolean quiet = false;
	private Map<String, Object> inParams;

	DataStore inDataStore;

	String tableName = "";
	Filter filter = Filter.INCLUDE;
	DeSpiker despiker = new DeSpiker();
	OracleFixer fixer = new OracleFixer();

	public static void main(String[] args) throws ParseException, IOException, CQLException {
		FixAndReplace me = new FixAndReplace();
		Map<String, Object> params = new HashMap<>();

		Options options = new Options();
		options.addOption("i", "inparams", true, "';' seperated parameter list");
		options.addOption("t", "table", true, "table name");
		options.addOption("v", "verbose", false, "more output");
		options.addOption("q", "quiet", false, "less output");
		options.addOption("f","filter",true,"A filter to restrict the features fetched");
		CommandLineParser parser = new DefaultParser();
		CommandLine cmd = parser.parse(options, args);
		if (cmd.hasOption("v")) {
			me.setVerbose(true);
			me.setQuiet(false);
		}
		if(cmd.hasOption('q')) {
			me.setQuiet(true);
			me.setVerbose(false);
		}
		if (cmd.hasOption('i')) {
			String p = cmd.getOptionValue('i');
			params = parseParameterString(p);
		}
		if (cmd.hasOption('t')) {
			me.setTableName(cmd.getOptionValue('t'));
		}
		
		if(cmd.hasOption('f')) {
			String cql = cmd.getOptionValue('f');
			me.setFilter(ECQL.toFilter(cql));
		}
		me.setInputParams(params);

		me.process();
	}

	private void setTableName(String value) {
		this.tableName = value;

	}
	static final FilterFactory2 FF = CommonFactoryFinder.getFilterFactory2();
	private void process() throws IOException {
		// fetch features
		SimpleFeatureCollection inFeatures = null;

		
		Query query = new Query(tableName, filter);
		SimpleFeatureSource featureSource = inDataStore.getFeatureSource(tableName);
		inFeatures = featureSource.getFeatures(query);
		if(verbose){
			System.out.println("processing "+inFeatures.size()+" features");
		}
		if (featureSource instanceof SimpleFeatureStore) {
			SimpleFeatureStore outStore = (SimpleFeatureStore) featureSource;
			SimpleFeatureIterator features = null;
			try {
				features = inFeatures.features();

				while (features.hasNext()) {
					SimpleFeature f = features.next();

					Geometry g = (Geometry) f.getAttribute("GEOMETRY");
					if(g==null || g.isEmpty()) {
						if(verbose)
							System.out.println("skipping null/empty geom "+f.getID());
						continue;
					}
					// fix feature
					if(verbose)
						System.out.println(g);
					g.normalize();
					if(verbose)
						System.out.println("Post Normalise"+g);

					g = despiker.deDuplicate(g);
					if(verbose)
						System.out.println("Post dedup:"+g);

					g = despiker.despike(g);
					if(verbose)
						System.out.println("Post despike:"+g);
					
					g = fixer.checkAndFixOrder(g);
					if(verbose)
						System.out.println("Post order check intersect:"+g);
					
					//if you buffer when the winding is wrong it all falls apart!
					g = fixer.selfSnap(g);
					if(verbose)
						System.out.println("Post self intersect:"+g);
					
					

					// update table
					Transaction transaction = new DefaultTransaction("create");
					outStore.setTransaction(transaction);
					String[] atts = {"GEOMETRY","GEOMETRY_CHANGED"};
					try {
						//"GEOMETRY", g
						Filter fidFilter = FF.id(FF.featureId(f.getID()));
						
						outStore.modifyFeatures(atts,new Object[] {g,"Y"}, fidFilter);						
						if(!quiet)
							System.out.println("wrote feature " + f.getID());
						transaction.commit();
					} catch (Exception problem) {
						System.out.println("failed to write "+f.getID());
						problem.printStackTrace();
						transaction.rollback();
						
					} finally {
						transaction.close();
					}

				}
			} finally {
				features.close();
			}
		}else {
			System.out.println("unwritable store");
		}

	}

	private void setInputParams(Map<String, Object> params) throws IOException {
		this.inParams = params;
		inDataStore = DataStoreFinder.getDataStore(inParams);
		if (inDataStore == null) {
			System.out.println("failed to open store with " + params);
			
			System.exit(2);
		}
	}

	/**
	 * @param parameters
	 * @return
	 */
	static public HashMap<String, Object> parseParameterString(String parameters) {
		HashMap<String, Object> params = new HashMap<>();
		if (parameters.startsWith("@")) {// a file name
			parameters = parameters.substring(1);
			Properties props = new Properties();
			try {
				props.load(new FileReader(new File(parameters)));
				for(Entry<Object, Object> e:props.entrySet()) {
					params.put((String)e.getKey(), e.getValue());
				}
			} catch (IOException e) {
				e.printStackTrace();
				System.exit(2);
			}
		} else if (parameters.contains(";")) {// a collection of params
			String[] parts = parameters.split(";");
			for (String part : parts) {
				String[] toks = part.split("=");
				params.put(toks[0], toks[1]);
			}
		} else if (parameters.contains("=")) {
			String[] toks = parameters.split("=");
			params.put(toks[0], toks[1]);
		} else {

			URL url = null;
			try {
				url = new URL(parameters);

			} catch (MalformedURLException e) {

			}
			if (url == null) {
				File f = new File(parameters);
				// if (f.exists()) {
				url = DataUtilities.fileToURL(f);
				// }

			}
			params.put("url", url);
		}
		return params;
	}

	public boolean isVerbose() {
		return verbose;
	}

	public void setVerbose(boolean verbose) {
		this.verbose = verbose;
	}

	public boolean isQuiet() {
		return quiet;
	}

	public void setQuiet(boolean quiet) {
		this.quiet = quiet;
	}

	public void setFilter(Filter filter) {
		this.filter = filter;
	}
}
