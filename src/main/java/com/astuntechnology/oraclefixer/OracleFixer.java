package com.astuntechnology.oraclefixer;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.commons.lang3.ArrayUtils;
import org.geotools.data.DataStore;
import org.geotools.data.DataStoreFinder;
import org.geotools.data.DataUtilities;
import org.geotools.data.DefaultTransaction;
import org.geotools.data.Query;
import org.geotools.data.Transaction;
import org.geotools.data.simple.SimpleFeatureCollection;
import org.geotools.data.simple.SimpleFeatureIterator;
import org.geotools.data.simple.SimpleFeatureSource;
import org.geotools.data.simple.SimpleFeatureStore;
import org.geotools.factory.GeoTools;
import org.geotools.factory.Hints;
import org.geotools.feature.simple.SimpleFeatureTypeBuilder;
import org.geotools.jdbc.JDBCDataStoreFactory;
import org.geotools.referencing.CRS;
import org.opengis.feature.simple.SimpleFeature;
import org.opengis.feature.simple.SimpleFeatureType;
import org.opengis.feature.type.AttributeDescriptor;
import org.opengis.filter.Filter;
import org.opengis.filter.identity.FeatureId;
import org.opengis.referencing.FactoryException;
import org.opengis.referencing.crs.CoordinateReferenceSystem;

import com.vividsolutions.jts.algorithm.CGAlgorithms;
import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.Geometry;
import com.vividsolutions.jts.geom.GeometryFactory;
import com.vividsolutions.jts.geom.LinearRing;
import com.vividsolutions.jts.geom.MultiPolygon;
import com.vividsolutions.jts.geom.Polygon;
import com.vividsolutions.jts.io.WKTReader;
import com.vividsolutions.jts.io.WKTWriter;
import com.vividsolutions.jts.operation.overlay.snap.GeometrySnapper;

public class OracleFixer {
	GeometryFactory gf = new GeometryFactory();
	DataStore datastore;
	/**
	 * don't make any changes just report how many features would be changed.
	 */
	private boolean dryrun = false;
	private boolean despike = false;
	private boolean deintersect = false;
	private DataStore output;

	public OracleFixer(DataStore ds, DataStore ods) {
		datastore = ds;
		output = ods;
	}

	public OracleFixer() {
		// TODO Auto-generated constructor stub
	}

	public static void main(String[] args)
			throws ParseException, IOException, com.vividsolutions.jts.io.ParseException {
		String host = "localhost";
		String port = "1521";
		String schema = "";
		String outSchema = "";
		String type = "oracle";
		String user = "";
		boolean check = false;
		boolean spike = false;
		boolean inter = false;
		Map<String, Object> params = new HashMap<String, Object>();
		Options options = new Options();
		options.addOption("h", "host", true, "Hostname (default " + host + ")");
		options.addOption("p", "port", true, "Port number (default " + port + ")");
		options.addOption("s", "schema", true, "Schema ");
		options.addOption("o", "outschema", true, "Output Schema ");
		options.addOption("d", "database", true, "Database name ");
		options.addOption("u", "user", true, "User Name ");
		options.addOption("", "type", true, "Database type (" + type + ") ");
		options.addOption("t", "table", true, "Name of table to fix");
		options.addOption("c", "check", false, "Just check the table to see if polygons need fixing");
		options.addOption("password", true, "Password ");
		options.addOption("w", "wkt", true, "use the polygon in WKT (@filename or WKT as arg)");
		options.addOption("", "despike", false, "remove spikes from the polygon");
		options.addOption("i", "inter", false, "remove self intersections from the polygon");

		CommandLineParser parser = new DefaultParser();
		CommandLine cmd = parser.parse(options, args);
		if (cmd.hasOption("h")) {
			host = cmd.getOptionValue("h");
		}
		params.put(JDBCDataStoreFactory.HOST.key, host);
		if (cmd.hasOption('p')) {
			port = cmd.getOptionValue('p');
		}
		params.put(JDBCDataStoreFactory.PORT.key, port);

		if (cmd.hasOption('u')) {
			user = cmd.getOptionValue('u');
			params.put(JDBCDataStoreFactory.USER.key, user);
		}
		if (cmd.hasOption("password")) {
			user = cmd.getOptionValue("password");
			params.put(JDBCDataStoreFactory.PASSWD.key, user);
		}

		if (cmd.hasOption('s')) {
			schema = cmd.getOptionValue('s');
			params.put(JDBCDataStoreFactory.SCHEMA.key, schema);
		}

		if (cmd.hasOption('d')) {
			schema = cmd.getOptionValue('d');
			params.put(JDBCDataStoreFactory.DATABASE.key, schema);
		}

		if (cmd.hasOption("type")) {
			type = cmd.getOptionValue("type");
		}
		params.put(JDBCDataStoreFactory.DBTYPE.key, type);

		String table = "";
		if (cmd.hasOption('t')) {
			table = cmd.getOptionValue('t');
		}
		if (cmd.hasOption('c')) {
			check = true;
		}
		if (cmd.hasOption("despike")) {
			spike = true;
		}
		if (cmd.hasOption("inter")) {
			inter = true;
		}
		if (cmd.hasOption('w')) {
			String wktFile = cmd.getOptionValue('w');
			WKTReader reader = new WKTReader();
			Geometry p;
			if (wktFile.startsWith("@")) {
				FileReader fileReader = new FileReader(new File(wktFile.substring(1)));
				p = reader.read(fileReader);
			} else {
				p = reader.read(wktFile);
			}
			OracleFixer f = new OracleFixer();
			if (spike) {
				DeSpiker ds = new DeSpiker();
				if (p instanceof Polygon) {
					Polygon po = ds.deDuplicate((Polygon) p);
					p = ds.despike(po);
				} else {
					MultiPolygon mp = (MultiPolygon) p;
					int n = mp.getNumGeometries();
					Polygon[] polygons = new Polygon[n];
					for (int i = 0; i < n; i++) {
						Polygon xp = (Polygon) mp.getGeometryN(i);
						polygons[i] = ds.deDuplicate(xp);
						System.out.println(
								"removed " + (xp.getNumPoints() - polygons[i].getNumPoints()) + " duplicate points");
						polygons[i] = ds.despike(polygons[i]);
					}
					p = f.gf.createMultiPolygon(polygons);
				}
			}
			if (inter) {

				p = f.selfSnap(p);

			}
			Geometry np = f.checkAndFixOrder(p);
			System.out.println("fixed " + (p != np));
			WKTWriter writer = new WKTWriter();
			System.out.println(writer.write(np));
			System.exit(0);
		}

		Map<String, Object> oparams = new HashMap<>();
		for (Entry<String, Object> e : params.entrySet()) {
			oparams.put(e.getKey(), e.getValue());
		}
		if (cmd.hasOption('o')) {
			outSchema = cmd.getOptionValue('o');
			oparams.put(JDBCDataStoreFactory.SCHEMA.key, outSchema);
		}
		DataStore ds = null;
		DataStore ods = null;
		try {

			ds = DataStoreFinder.getDataStore(params);
			ods = DataStoreFinder.getDataStore(oparams);
		} catch (IOException e) {

			e.printStackTrace();
			System.exit(1);
		}
		if (ds == null) {
			System.out.println("unable to open Database\n" + params);
			System.exit(1);
		}

		if (ods == null) {
			System.out.println("unable to open output Database\n" + oparams);
			System.exit(1);
		}

		OracleFixer fixer = new OracleFixer(ds, ods);
		fixer.setDryrun(check);
		fixer.setFixIntersections(inter);
		fixer.setDeSpike(spike);
		if (!table.isEmpty()) {
			fixer.fixTable(table);
		} else {
			String[] names = ds.getTypeNames();
			System.out.println("tables (" + names.length + "):");
			for (String n : names) {
				System.out.println("\t" + n);
			}
		}
	}

	public Geometry checkAndFixOrder(Geometry p) {
		if (p instanceof Polygon) {
			return checkAndFixOrder((Polygon) p);
		} else if (p instanceof MultiPolygon) {
			MultiPolygon mp = (MultiPolygon) p;
			int n = mp.getNumGeometries();
			Polygon[] polygons = new Polygon[n];
			for (int i = 0; i < n; i++) {
				Polygon poly = (Polygon) mp.getGeometryN(i);
				polygons[i] = checkAndFixOrder(poly);

			}
			return gf.createMultiPolygon(polygons);
		}
		return p;
	}

	public void setFixIntersections(boolean inter) {
		deintersect = true;

	}

	private void setDeSpike(boolean spike) {
		despike = spike;

	}

	private void setDryrun(boolean check) {
		this.dryrun = check;

	}

	public boolean fixTable(String table) throws IOException {
		SimpleFeatureCollection inFeatures = null;

		Query query = new Query(table, Filter.INCLUDE);
		inFeatures = datastore.getFeatureSource(table).getFeatures(query);
		ArrayList<SimpleFeature> list = examineFeatures(inFeatures);
		if (dryrun) {
			return true;
		}
		SimpleFeatureType schema = inFeatures.getSchema();
		if (table.length() > 26) {
			table = table.substring(0, 26);
		}
		String schemaName = table + "_NEW";
		SimpleFeatureType newSchema = copySchema(schema, schemaName);

		boolean exists = false;

		String[] typeNames = output.getTypeNames();
		for (String name : typeNames) {
			if (schemaName.equalsIgnoreCase(name)) {
				exists = true;
			}
		}
		if (exists) {
			output.removeSchema(newSchema.getName().getLocalPart());
		}
		System.out.println("creating table " + newSchema.getName().getLocalPart());
		output.createSchema(newSchema);

		Transaction transaction = new DefaultTransaction("create");
		SimpleFeatureSource featureSource = output.getFeatureSource(schemaName);
		if (featureSource instanceof SimpleFeatureStore) {

			SimpleFeatureCollection collection = DataUtilities.collection(list);
			SimpleFeatureStore outStore = (SimpleFeatureStore) featureSource;
			outStore.setTransaction(transaction);

			try {
				List<FeatureId> ids = outStore.addFeatures(collection);
				System.out.println("wrote " + ids.size() + " features");
				transaction.commit();
			} catch (Exception problem) {
				problem.printStackTrace();
				transaction.rollback();
			} finally {
				transaction.close();
			}

			return true;
		} else {
			transaction.close();

			System.err.println("Output datastore (" + datastore + ") not writable");

			return false;
		}

	}

	DeSpiker deSpiker = new DeSpiker();

	private ArrayList<SimpleFeature> examineFeatures(SimpleFeatureCollection inFeatures) {
		SimpleFeatureIterator features = null;
		try {
			features = inFeatures.features();
			ArrayList<SimpleFeature> list = new ArrayList<>();
			int count = 0;
			while (features.hasNext()) {
				SimpleFeature f = features.next();

				Geometry g = (Geometry) f.getDefaultGeometry();
				if (!g.isValid() && g instanceof Polygon) {
					Polygon ng = doMagic(g);
					if (g != ng) {
						count++;
						if (!dryrun) {
							f.setDefaultGeometry(ng);
						}
					}

				} else if (!g.isValid() && g instanceof MultiPolygon) {
					MultiPolygon mp = ((MultiPolygon) g);
					ArrayList<Polygon> polys = new ArrayList<>();
					int len = mp.getNumGeometries();
					boolean changed = false;
					for (int i = 0; i < len; i++) {
						Geometry og = mp.getGeometryN(i);
						Polygon ng = doMagic(og);
						if (og != ng) {
							changed = true;
						}
						polys.add(ng);
					}
					if (changed) {
						count++;

						MultiPolygon out = gf.createMultiPolygon(polys.toArray(new Polygon[] {}));
						f.setDefaultGeometry(out);
					}
				}

				list.add(f);
			}
			System.out.println("Updated " + count + " geometries");
			return list;
		} finally {
			if (features != null) {
				features.close();
			}
		}
	}

	private Polygon doMagic(Geometry g) {
		if (despike) {
			Polygon n = (Polygon) g;
			n.normalize();
			Polygon x = deSpiker.deDuplicate(n);
			Polygon p = deSpiker.despike(x);
			g = p;
		}
		Polygon ng = checkAndFixOrder((Polygon) g);
		Polygon ig = ng;
		if (deintersect) {
			ig = (Polygon) selfSnap(ng);
		}
		return ig;
	}

	private SimpleFeatureType copySchema(SimpleFeatureType schema, String name) {
		// annoyingly there is no quick easy way to do this!
		List<AttributeDescriptor> attrs = schema.getAttributeDescriptors();
		SimpleFeatureTypeBuilder builder = new SimpleFeatureTypeBuilder();
		builder.setName(name);
		CoordinateReferenceSystem coordinateReferenceSystem = schema.getCoordinateReferenceSystem();
		if (coordinateReferenceSystem != null) {
			// System.out.println("setting crs to " +
			// coordinateReferenceSystem);
		} else {
			try {
				coordinateReferenceSystem = CRS.decode("epsg:27700");
				System.out.println("forcing CRS to EPSG:27700");
			} catch (FactoryException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		builder.setCRS(coordinateReferenceSystem);
		builder.setDefaultGeometry(schema.getGeometryDescriptor().getLocalName());
		builder.setAttributes(attrs);
		return builder.buildFeatureType();
	}

	private Polygon checkAndFixOrder(Polygon in) {
		boolean change = false;
		Coordinate[] shellCoords = in.getExteriorRing().getCoordinates();
		if (shellCoords.length < 4) {
			// System.out.println("invalid polygon " + in);
			return gf.createPolygon(null, null);
		}
		if (!CGAlgorithms.isCCW(shellCoords)) { // SF Exterior rings should be
												// counter clockwise
			// reverse
			change = true;
			ArrayUtils.reverse(shellCoords);
		}
		LinearRing shell = gf.createLinearRing(shellCoords);

		LinearRing[] holes = new LinearRing[in.getNumInteriorRing()];
		for (int i = 0; i < in.getNumInteriorRing(); i++) {

			Coordinate[] coordinates = in.getInteriorRingN(i).getCoordinates();

			if (coordinates.length > 3 && CGAlgorithms.isCCW(coordinates)) {// SF
																			// holes
																			// are
																			// clockwise
				// reverse
				ArrayUtils.reverse(coordinates);
				change = true;
			}

			holes[i] = gf.createLinearRing(coordinates);
		}
		if (!change)
			return in;
		else
			return gf.createPolygon(shell, holes);
	}
	// self-snap the geometries, to eliminate "zero-width spikes"

	public Geometry selfSnap(Geometry g) {
		double snapTolerance = 0.005;

		if(!(g instanceof Polygon || g instanceof MultiPolygon)) {
			return g;
		}
		GeometrySnapper snapper = new GeometrySnapper(g);
		Geometry snapped = snapper.snapTo(g, snapTolerance);
		// need to "clean" snapped geometry - use buffer(0) as a simple way to
		// do this
		Geometry fix = snapped.buffer(0);
		return fix;
	}
}
