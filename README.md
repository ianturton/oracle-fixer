# README #

These tools carry out a series of operations to remove or correct common errors seen in invalid geometries.

## Remove spikes
  
A spike is a relatively short, narrow excursion from the polygon usually caused by the digitisation process.
     
![spike](./spike.png)

The tool uses the same [algorithm](http://gasparesganga.com/labs/postgis-normalize-geometry/) as is used by PostGIS to detect small
area  triangles with narrow bases. These are then deleted if they fall below a specified threshold.
      
## Remove duplicate points

When digitising polygons it is possible to create several points in the same spot (or very close) that Oracle is unable to handle. 
This tool removes them based on a tolerance setting which determines what is considered close.

## Correct orientation of exterior and interior rings

Oracle (and the OGC Simple Feature standard) expects that the exterior ring of a polygon is orientated 
anti-clockwise and that interior rings (holes) are orientated clockwise. These tools inspect each polygon in the table and check 
for  violations of these rules and fix them by reversing the order of problem polygons.

## Self Intersecting Polygons

Oracle (and the OGC Simple Feature standard) does not permit a polygon's outer boundary to cross itself.

![bowtie](bowtie.png)

In this example the four corners of the polygon have been digitised in the wrong order so that the boundary crosses itself. This tool attempts to fix these 
errors by creating an infinitesimal buffer around the feature and taking that boundary. In the case of the "bow tie" shown here it creates two triangles with the correct 
orientation **not** the square intended. However in cases of smaller digitisation errors it usually creates a correct polygon.
